//
//  Application.h
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import "AppDelegate.h"
#import <Foundation/Foundation.h>

@interface Application : NSObject

@property (nonatomic, readonly) AppDelegate *appDelegate;

- (void)startWithAppDelegate:(AppDelegate *)delegate;
- (void)pause;
- (void)resume;
- (void)stop;

@end
