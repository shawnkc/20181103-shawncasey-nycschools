//
//  Application.m
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import "Application.h"
#import "ListingViewController.h"
#import "ModelController.h"
#import "Util.h"

@interface Application()

@property (nonatomic, readwrite) AppDelegate *appDelegate;

@property ModelController *modelController;

@end

// Makes the app delegate clean and allows us to reuse the Application object in other projects
@implementation Application

- (void)startWithAppDelegate:(AppDelegate *)delegate {
    self.appDelegate = delegate;

    // This will override the main view controller after loading is done and set us up for dependency injection below
    __block UIViewController *primaryViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Primary"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.appDelegate setMainViewController:primaryViewController];
    });

    // Assuming both files have the same encoding
    NSString *schoolDataFile = [Util getPathFromBundle:@"2017_DOE_High_School_Directory.csv"];
    NSString *satDataFile = [Util getPathFromBundle:@"2012_SAT_Results.csv"];
    self.modelController = [[ModelController alloc] initWithSchoolData:schoolDataFile
                                                               satData:satDataFile
                                                              encoding:NSUTF8StringEncoding];
    [self.modelController loadWithCompletion:^(BOOL success) {
        // Use dependency injection for the model controller
        if ([primaryViewController isKindOfClass:[UINavigationController class]]) {
            primaryViewController = ((UINavigationController *)primaryViewController).topViewController;
            if ([primaryViewController respondsToSelector:@selector(setModelController:)]) {
                [primaryViewController performSelector:@selector(setModelController:) withObject:self.modelController];
            }
        } else if ([primaryViewController isKindOfClass:[UITabBarController class]]) {
            for (UIViewController *vc in ((UITabBarController *)primaryViewController).viewControllers) {
                if ([vc respondsToSelector:@selector(setModelController:)]) {
                    [vc performSelector:@selector(setModelController:) withObject:self.modelController];
                }
            }
        } else {
            if ([primaryViewController respondsToSelector:@selector(setModelController:)]) {
                [primaryViewController performSelector:@selector(setModelController:) withObject:self.modelController];
            }
        }
    }];
}

- (void)pause {
    [self.modelController saveWithCompletion:nil];
}

- (void)resume {
}

- (void)stop {
    [self.modelController saveWithCompletion:nil];
}

@end
