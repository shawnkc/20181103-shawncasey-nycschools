//
//  SatHistory.h
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SatHistory : NSObject

// input - Unformatted CSV from the input datastream
// keys - Array of keys signifying each field of the CSV
// See: https://data.cityofnewyork.us/Education/2012-SAT-Results/f9bf-2cp4
- (instancetype)initWithString:(NSString *)input keys:(NSArray<NSString *> *)keys;

@property (readonly, nonatomic) int numTakers;
@property (readonly, nonatomic) int avgReadingScore;
@property (readonly, nonatomic) int avgMathScore;
@property (readonly, nonatomic) int avgWritingScore;
@property (readonly, nonatomic) NSString *name;
@property (readonly, nonatomic) NSString *dbn;

// Each object has a shared reference to the keys
@property (readonly, nonatomic) NSArray<NSString *> *keys;

@end
