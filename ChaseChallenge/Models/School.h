//
//  School.h
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SatHistory.h"

@interface School : NSObject

// input - Unformatted CSV from the input datastream
// keys - Array of keys signifying each field of the CSV
// See: https://data.cityofnewyork.us/Education/2017-DOE-High-School-Directory/s3k6-pzi2
- (instancetype)initWithString:(NSString *)input keys:(NSArray<NSString *> *)keys;

- (void)attachSatHistory:(SatHistory *)satHistory;

@property (readonly, nonatomic) NSString *dbn;
@property (readonly, nonatomic) NSString *name;
@property (readonly, nonatomic) NSString *city;
@property (readonly, nonatomic) NSString *address;
@property (readonly, nonatomic) NSString *state;
@property (readonly, nonatomic) NSString *zip;
@property (readonly, nonatomic) NSString *phone;
@property (readonly, nonatomic) NSString *website;
@property (readonly, nonatomic) NSNumber *latitude;
@property (readonly, nonatomic) NSNumber *longitude;
@property (readonly, nonatomic) int totalStudents;
@property (readonly, nonatomic) SatHistory *satHistory;

// Each object has a shared reference to the keys 
@property (readonly, nonatomic) NSArray<NSString *> *keys;

@end
