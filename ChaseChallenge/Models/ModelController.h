//
//  ModelController.h
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "School.h"
#import "SatHistory.h"

typedef NS_ENUM(NSInteger, SchoolSortType) {
    SchoolSortTypeName,
    SchoolSortTypeSatMath,
    SchoolSortTypeSatReading,
    SchoolSortTypeSatWriting,
    SchoolSortTypeSatTakers,
    SchoolSortTypeStudents,
    SchoolSortTypeCount,        // Always LAST!
};

@interface ModelController : NSObject

- (instancetype)initWithSchoolData:(NSString *)schoolFilename satData:(NSString *)satFilename encoding:(NSStringEncoding)encoding;

- (void)loadWithCompletion:(void(^)(BOOL success))completion;
- (void)saveWithCompletion:(void(^)(BOOL success))completion;

// Returns sorted array of schools asynchronously, this will cache each type of sort
- (void)getSchoolsSortedBy:(SchoolSortType)sortType completion:(void(^)(NSArray *schools))completion;

// Returns a string for each item in the array, if enum doesn't match, object:load should throw an error
+ (NSString *)stringForSortType:(SchoolSortType)sortType;

@property (readonly, nonatomic) BOOL isLoaded;

@end
