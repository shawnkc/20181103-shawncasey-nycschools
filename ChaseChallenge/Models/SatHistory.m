//
//  SatHistory.m
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import "SatHistory.h"
#import "Util.h"

@interface SatHistory()

@property (readwrite, nonatomic) NSArray<NSString *> *keys;

@property (nonatomic) NSMutableDictionary *rawData;

@end

@implementation SatHistory


- (instancetype)initWithString:(NSString *)input keys:(NSArray<NSString *> *)keys {
    if (self = [super init]) {
        self.rawData = [NSMutableDictionary new];
        
        if (![self parseDataWithString:input keys:keys]) {
            return nil;
        }
        self.keys = keys;
    }
    return self;
}

- (NSString *)dbn {
    return self.rawData[@"DBN"];
}

- (NSString *)name {
    return self.rawData[@"SCHOOL NAME"];
}

- (int)numTakers {
    return [NSNUMBER_OR_ZERO(self.rawData[@"Num of SAT Test Takers"]) intValue];
}

- (int)avgReadingScore {
    return [NSNUMBER_OR_ZERO(self.rawData[@"SAT Critical Reading Avg. Score"]) intValue];
}

- (int)avgMathScore {
    return [NSNUMBER_OR_ZERO(self.rawData[@"SAT Math Avg. Score"]) intValue];
}

- (int)avgWritingScore {
    return [NSNUMBER_OR_ZERO(self.rawData[@"SAT Writing Avg. Score"]) intValue];
}

#pragma mark - Private methods

- (BOOL)parseDataWithString:(NSString *)input keys:(NSArray<NSString *> *)keys {
    NSParameterAssert(keys);
    NSParameterAssert(input);
    
    if (!keys || !input) {
        return FALSE;
    }
    
    NSArray *cleanedColumns = [Util parseCleanColumnsFromCSVLine:input];
    for (int i = 0; i < cleanedColumns.count; i++) {
        NSString *column = cleanedColumns[i];
        NSString *key = [keys objectAtIndex:i];
        if (!column || !key) {
            NSLog(@"Error, invalid data for: %@...", [input substringToIndex:20]);
            NSLog(@"\tColumn: %@", column);
            NSLog(@"\tKey: %@", key);
            continue;
        }
        if (i >= 2) {
            [self.rawData setObject:@([column intValue]) forKey:key];
        } else {
            [self.rawData setObject:column forKey:key];
        }
    }
    
    return TRUE;
}

@end
