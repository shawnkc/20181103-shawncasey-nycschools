//
//  School.m
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import "School.h"
#import "Util.h"

@interface School()

@property (readwrite, nonatomic) SatHistory *satHistory;
@property (readwrite, nonatomic) NSArray<NSString *> *keys;

@property NSMutableDictionary *rawData;

@end

@implementation School

- (instancetype)initWithString:(NSString *)input keys:(NSArray<NSString *> *)keys {
    if (self = [super init]) {
        self.rawData = [NSMutableDictionary new];

        if (![self parseDataWithString:input keys:keys]) {
            return nil;
        }
        self.keys = keys;
    }
    return self;
}

- (void)attachSatHistory:(SatHistory *)satHistory {
    self.satHistory = satHistory;
}

- (NSString *)dbn {
    return self.rawData[@"dbn"];
}

- (NSString *)name {
    return self.rawData[@"school_name"];
}

- (NSString *)city {
    return self.rawData[@"city"];
}

- (NSString *)address {
    return self.rawData[@"primary_address_line_1"];
}

- (NSString *)state {
    return self.rawData[@"state_code"];
}

- (NSString *)zip {
    return self.rawData[@"Postcode"];
}

- (NSString *)phone {
    return self.rawData[@"phone_number"];
}

- (NSString *)website {
    return self.rawData[@"website"];
}

- (int)totalStudents {
    return ((NSString *)self.rawData[@"total_students"]).intValue;
}

- (NSNumber *)latitude {
    return NSNUMBER_OR_NIL(self.rawData[@"latitude"]);
}

- (NSNumber *)longitude {
    return NSNUMBER_OR_NIL(self.rawData[@"longitude"]);
}

#pragma mark - Private methods

- (BOOL)parseDataWithString:(NSString *)input keys:(NSArray<NSString *> *)keys {
    NSParameterAssert(keys);
    NSParameterAssert(input);
    
    if (!keys || !input) {
        return FALSE;
    }
    
    NSArray *cleanedColumns = [Util parseCleanColumnsFromCSVLine:input];
    for (int i = 0; i < cleanedColumns.count; i++) {
        NSString *column = cleanedColumns[i];
        NSString *key = [keys objectAtIndex:i];
        if (!column || !key) {
            NSLog(@"Error, invalid data for: %@...", [input substringToIndex:20]);
            NSLog(@"\tColumn: %@", column);
            NSLog(@"\tKey: %@", key);
            continue;
        }
        [self.rawData setObject:column forKey:key];
    }

    return TRUE;
}

@end
