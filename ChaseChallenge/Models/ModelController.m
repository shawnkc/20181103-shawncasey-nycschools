//
//  ModelController.m
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import "ModelController.h"
#import "School.h"
#import "SatHistory.h"
#import "Util.h"

@interface ModelController()

@property (nonatomic) NSString *schoolFilename;
@property (nonatomic) NSString *satFilename;

@property (nonatomic) NSMutableDictionary *sortedCache;
@property (nonatomic) NSStringEncoding encoding;

@property (nonatomic) NSDictionary *schoolData;

@property (readwrite, nonatomic) BOOL isLoaded;

@end

@implementation ModelController

- (instancetype)initWithSchoolData:(NSString *)schoolFilename satData:(NSString *)satFilename encoding:(NSStringEncoding)encoding {
    if (self = [super init]) {
        self.schoolFilename = schoolFilename;
        self.satFilename = satFilename;
        self.isLoaded = FALSE;
        self.encoding = encoding;
    }
    return self;
}

- (void)loadWithCompletion:(void(^)(BOOL success))completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // Reset everything
        self.sortedCache = [NSMutableDictionary new];

        NSDictionary *schoolData = nil;
        NSDictionary *satData = nil;
        
        @try {
            schoolData = [self loadSchoolData];
            satData = [self loadSatHistoryData];

            for (NSString *dbn in schoolData.allKeys) {
                School *school = schoolData[dbn];
                SatHistory *satHistory = [satData objectForKey:dbn];
                if (satHistory) {
                    [school attachSatHistory:satHistory];
                }
            }
        } @catch (NSException *exception) {
            NSLog(@"Error found during load: %@", exception.description);
        } @finally {
            self.isLoaded = TRUE;
            self.schoolData = schoolData;
            
            if (completion) {
                completion(self.isLoaded);
            }
        }
    });
}

- (void)saveWithCompletion:(void(^)(BOOL success))completion {
    // NOTE: All our data right now is readonly, nothing to do here
    
    if (completion) {
        completion(TRUE);
    }
}

- (void)getSchoolsSortedBy:(SchoolSortType)sortType completion:(void(^)(NSArray *schools))completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSArray *sorted = [self.sortedCache objectForKey:@(sortType)];
        
        if (!sorted) {
            NSArray *unsorted = self.schoolData.allValues;
            sorted = [unsorted sortedArrayUsingComparator:^NSComparisonResult(School *obj1, School *obj2) {
                if (sortType == SchoolSortTypeName) {
                    return [obj1.name compare:obj2.name];
                } else if (sortType == SchoolSortTypeStudents) {
                    return obj2.totalStudents - obj1.totalStudents;
                } else if (sortType == SchoolSortTypeSatTakers) {
                    if (obj1.satHistory && obj2.satHistory) {
                        return obj2.satHistory.numTakers - obj1.satHistory.numTakers;
                    } else if (obj1.satHistory) {
                        return NSOrderedAscending;
                    } else if (obj2.satHistory) {
                        return NSOrderedDescending;
                    } else {
                        return NSOrderedSame;
                    }
                } else if (sortType == SchoolSortTypeSatMath) {
                    if (obj1.satHistory && obj2.satHistory) {
                        return obj2.satHistory.avgMathScore - obj1.satHistory.avgMathScore;
                    } else if (obj1.satHistory) {
                        return NSOrderedAscending;
                    } else if (obj2.satHistory) {
                        return NSOrderedDescending;
                    } else {
                        return NSOrderedSame;
                    }
                } else if (sortType == SchoolSortTypeSatReading) {
                    if (obj1.satHistory && obj2.satHistory) {
                        return obj2.satHistory.avgReadingScore - obj1.satHistory.avgReadingScore;
                    } else if (obj1.satHistory) {
                        return NSOrderedAscending;
                    } else if (obj2.satHistory) {
                        return NSOrderedDescending;
                    } else {
                        return NSOrderedSame;
                    }
                } else if (sortType == SchoolSortTypeSatWriting) {
                    if (obj1.satHistory && obj2.satHistory) {
                        return obj2.satHistory.avgWritingScore - obj1.satHistory.avgWritingScore;
                    } else if (obj1.satHistory) {
                        return NSOrderedAscending;
                    } else if (obj2.satHistory) {
                        return NSOrderedDescending;
                    } else {
                        return NSOrderedSame;
                    }
                }
                
                return NSOrderedSame;
            }];
            
            // Save for next time
            [self.sortedCache setObject:sorted forKey:@(sortType)];
        }
        
        if (completion) {
            completion(sorted);
        }
    });
}

#pragma mark - Private methods

- (NSDictionary *)loadSchoolData {
    NSMutableDictionary *schoolData = [NSMutableDictionary new];
    
    NSError *error;
    NSString *data = [NSString stringWithContentsOfFile:self.schoolFilename encoding:self.encoding error:&error];
    if (data && !error) {
        NSArray *rows = [data componentsSeparatedByString:@"\n"];
        NSArray *keys = nil;
        for (NSString *row in rows) {
            NSArray *columns = [row componentsSeparatedByString:@","];
            if (!keys) {
                keys = columns;
                continue;
            }
            
            if (IS_STRING_SET(row)) {
                // Let the object determine how to parse in it's own class
                School *school = [[School alloc] initWithString:row keys:keys];
                if (school && school.dbn) {
                    [schoolData setObject:school forKey:school.dbn];
                }
            }
        }
    } else {
        if (error) {
            // TODO: propogate error for visual display?
            // TODO: use CocoaLumberjack or some other logging framework?
            NSLog(@"Error loading SAT data: %@", error.localizedDescription);
        } else if (!data) {
            NSLog(@"Warning, no data found in: %@", self.satFilename);
        }
    }
    
    return schoolData;
}

- (NSDictionary *)loadSatHistoryData {
    NSMutableDictionary *satData = [NSMutableDictionary new];
    
    NSError *error;
    NSString *data = [NSString stringWithContentsOfFile:self.satFilename encoding:self.encoding error:&error];
    if (data && !error) {
        NSArray *rows = [data componentsSeparatedByString:@"\n"];
        NSArray *keys = nil;
        for (NSString *row in rows) {
            NSArray *columns = [row componentsSeparatedByString:@","];
            if (!keys) {
                keys = columns;
                continue;
            }

            if (IS_STRING_SET(row)) {
                // Let the object determine how to parse in it's own class
                SatHistory *satHistory = [[SatHistory alloc] initWithString:row keys:keys];
                if (satHistory && satHistory.dbn) {
                    [satData setObject:satHistory forKey:satHistory.dbn];
                }
            }
        }
    } else {
        if (error) {
            // TODO: propogate error for visual display?
            // TODO: use CocoaLumberjack or some other logging framework?
            NSLog(@"Error loading SAT data: %@", error.localizedDescription);
        } else if (!data) {
            NSLog(@"Warning, no data found in: %@", self.satFilename);
        }
    }
    
    return satData;
}

+ (NSString *)stringForSortType:(SchoolSortType)sortType {
    NSDictionary *dict = @{@(SchoolSortTypeName): @"School Name",
                           @(SchoolSortTypeSatTakers) : @"SAT Takers",
                           @(SchoolSortTypeStudents) : @"Students",
                           @(SchoolSortTypeSatMath) : @"SAT Math",
                           @(SchoolSortTypeSatReading) : @"SAT Reading",
                           @(SchoolSortTypeSatWriting) : @"SAT Writing"};
    return [dict objectForKey:@(sortType)];
}

+ (void)load {
    // Check the string method to make sure it handles the enums (at least the number of them)
    for (int i = 0; i < SchoolSortTypeCount; i++) {        
        if ([self stringForSortType:i] == nil) {
            @throw @"stringForSortType/enum mismatch";
        }
    }
}

@end
