//
//  ListingTableViewCell.h
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "School.h"

@interface ListingTableViewCell : UITableViewCell

@property (nonatomic) School *school;

@end
