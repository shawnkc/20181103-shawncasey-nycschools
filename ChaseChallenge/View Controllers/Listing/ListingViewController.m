//
//  ListingViewController.m
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import "ListingViewController.h"
#import "ListingTableViewCell.h"
#import "SchoolDetailViewController.h"
#import "LGFilterView.h"
#import "Util.h"

@interface ListingViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSArray<School *> *schoolData;
@property (nonatomic) NSArray<School *> *filteredSchoolData;

@property (nonatomic) School *selectedSchool;

// For the sorting
@property (nonatomic) SchoolSortType schoolSort;
@property (nonatomic) LGFilterView *filterView;
@property (nonatomic) NSMutableArray *titlesArray;

// For searching/filtering
@property (nonatomic) UISearchController *searchController;

@end

@implementation ListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    if (!self.modelController.isLoaded) {
        [self addObserver:self.modelController forKeyPath:@"isLoaded" options:NSKeyValueObservingOptionNew context:nil];
    } else {
        [self selectSchoolSort:SchoolSortTypeName];
    }
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.hidesNavigationBarDuringPresentation = false;
    self.searchController.dimsBackgroundDuringPresentation = false;
    
    self.navigationItem.searchController = self.searchController;
}

- (void)setupFilterView {
    if (!self.filterView) {
        UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Sort"] style:UIBarButtonItemStylePlain target:self action:@selector(pressSortButton:)];
        self.navigationItem.rightBarButtonItem = rightButton;
        
        self.titlesArray = [NSMutableArray new];
        for (int i = 0; i < SchoolSortTypeCount; i++) {
            [self.titlesArray addObject:[ModelController stringForSortType:i]];
        }
        
        self.filterView = [[LGFilterView alloc] initWithTitles:self.titlesArray actionHandler:^(LGFilterView *filterView, NSString *title, NSUInteger index) {
            [self selectSchoolSort:(SchoolSortType)index];
        } cancelHandler:nil];
        self.filterView.transitionStyle = LGFilterViewTransitionStyleTop;
        self.filterView.numberOfLines = 0;
        self.filterView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
        self.filterView.offset = CGPointZero;
        self.filterView.heightMax = 300.0f;
    }
}

- (void)pressSortButton:(UIButton *)button {
    if (!self.filterView.isShowing) {
        self.filterView.selectedIndex = self.schoolSort;
        [self.filterView showInView:self.view animated:YES completionHandler:nil];
    } else {
        [self.filterView dismissAnimated:YES completionHandler:nil];
    }
}

- (void)setModelController:(ModelController *)modelController {
    _modelController = modelController;
    [self selectSchoolSort:SchoolSortTypeName];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setupFilterView];
    });
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"isLoaded"]) {
        [self selectSchoolSort:SchoolSortTypeName];
    } else {
        // Any unrecognized context must belong to super
        [super observeValueForKeyPath:keyPath
                             ofObject:object
                               change:change
                              context:context];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[SchoolDetailViewController class]]) {
        SchoolDetailViewController *sdvc = (SchoolDetailViewController *)segue.destinationViewController;
        sdvc.school = self.selectedSchool;
    }
}

#pragma mark -

- (void)selectSchoolSort:(SchoolSortType)schoolSort {
    self.schoolSort = schoolSort;
    
    // TODO: show progress dialog?
    
    if (self.modelController) {
        [self.modelController getSchoolsSortedBy:schoolSort completion:^(NSArray *schools) {
            self.schoolData = schools;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Handles both the filtered and non-filtered cases
                [self updateSearchResultsForSearchController:self.searchController];
            });
        }];
    }
}

#pragma mark - Table View Delegate

#pragma mark - Table View Datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ListingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListingTableViewCell"];
    if (self.filteredSchoolData) {
        cell.school = self.filteredSchoolData[indexPath.row];
    } else {
        cell.school = self.schoolData[indexPath.row];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.filteredSchoolData) {
        return self.filteredSchoolData.count;
    }
    return self.schoolData.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    
    if (self.filteredSchoolData) {
        self.selectedSchool = self.filteredSchoolData[indexPath.row];
    } else {
        self.selectedSchool = self.schoolData[indexPath.row];
    }

    [self performSegueWithIdentifier:@"showDetail" sender:nil];
}

#pragma mark - Search Results

// Called when the search bar's text or scope has changed or when the search bar becomes first responder.
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchText = searchController.searchBar.text.lowercaseString;
    
    if (IS_STRING_SET(searchText)) {
        self.filteredSchoolData = [self.schoolData filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(School *school, NSDictionary *bindings) {
            return [school.name.lowercaseString containsString:searchText] || [school.city.lowercaseString containsString:searchText] || [school.address.lowercaseString containsString:searchText];
        }]];
    } else {
        self.filteredSchoolData = nil;
    }
    
    [self.tableView reloadData];
}

@end
