//
//  ListingTableViewCell.m
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import "ListingTableViewCell.h"

@interface ListingTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *labelSchoolName;
@property (weak, nonatomic) IBOutlet UILabel *labelCity;
@property (weak, nonatomic) IBOutlet UILabel *labelSatData;
@property (weak, nonatomic) IBOutlet UILabel *labelStudents;

@end

@implementation ListingTableViewCell

- (void)setSchool:(School *)school {
    _school = school;
    self.labelSchoolName.text = school.name;
    self.labelCity.text = [NSString stringWithFormat:@"%@, %@", school.city, school.zip];
    if (school.satHistory && school.satHistory.numTakers > 0) {
        self.labelSatData.text = [NSString stringWithFormat:@"SAT scores: %d/%d/%d (Num results: %d)", school.satHistory.avgMathScore, school.satHistory.avgReadingScore, school.satHistory.avgWritingScore, school.satHistory.numTakers];
    } else {
        self.labelSatData.text = @"";
    }
    
    if (school.totalStudents > 0) {
        self.labelStudents.text = [NSString stringWithFormat:@"Student Size: %d", school.totalStudents];
    } else {
        self.labelStudents.text = @"";
    }
}

@end
