//
//  ListingViewController.h
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelController.h"

@interface ListingViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchControllerDelegate>

@property (nonatomic) ModelController *modelController;

@end

