//
//  SchoolDetailViewController.m
//  ChaseChallenge
//
//  Created by Shawn on 11/3/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import "SchoolDetailViewController.h"

@interface SchoolDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UITextView *textViewAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelCityStateZip;
@property (weak, nonatomic) IBOutlet UITextView *textViewPhone;
@property (weak, nonatomic) IBOutlet UITextView *textViewWebsite;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalStudents;
@property (weak, nonatomic) IBOutlet UILabel *labelSatAvgMath;
@property (weak, nonatomic) IBOutlet UILabel *labelSatAvgReading;
@property (weak, nonatomic) IBOutlet UILabel *labelSatAvgWriting;
@property (weak, nonatomic) IBOutlet UILabel *labelSatParticipants;
@property (weak, nonatomic) IBOutlet UIView *viewSatContainer;
@property (weak, nonatomic) IBOutlet UIView *viewNoSatContainer;

@end

@implementation SchoolDetailViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateViews];
}

#pragma mark -

- (void)updateViews {
    self.labelName.text = self.school.name;
    self.textViewAddress.text = [NSString stringWithFormat:@"%@\n%@, %@ %@", self.school.address, self.school.city, self.school.state, self.school.zip];
    self.textViewPhone.text = self.school.phone;
    self.textViewWebsite.text = self.school.website;
    if (self.school.totalStudents > 0) {
        self.labelTotalStudents.text = [NSString stringWithFormat:@"Students: %d", self.school.totalStudents];
    } else {
        self.labelTotalStudents.text = @"";
    }

    if (self.school.satHistory && self.school.satHistory.numTakers > 0) {
        self.viewSatContainer.hidden = FALSE;
        self.viewNoSatContainer.hidden = TRUE;
        self.labelSatAvgMath.text = [NSString stringWithFormat:@"%d", self.school.satHistory.avgMathScore];
        self.labelSatAvgReading.text = [NSString stringWithFormat:@"%d", self.school.satHistory.avgReadingScore];
        self.labelSatAvgWriting.text = [NSString stringWithFormat:@"%d", self.school.satHistory.avgWritingScore];
        self.labelSatParticipants.text = [NSString stringWithFormat:@"%d results", self.school.satHistory.numTakers];
    } else {
        self.viewSatContainer.hidden = TRUE;
        self.viewNoSatContainer.hidden = FALSE;
    }
}

@end
