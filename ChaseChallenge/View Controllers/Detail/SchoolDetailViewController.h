//
//  SchoolDetailViewController.h
//  ChaseChallenge
//
//  Created by Shawn on 11/3/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "School.h"

@interface SchoolDetailViewController : UIViewController

@property (nonatomic) School *school;

@end
