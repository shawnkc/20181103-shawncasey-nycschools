//
//  ListingTableViewCell.m
//  ChaseChallenge
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import "ListingTableViewCell.h"

@interface ListingTableViewCell()

@property (weak, nonatomic) UILabel *labelSchoolName;
@property (weak, nonatomic) UILabel *labelCity;
@property (weak, nonatomic) UILabel *labelSatData;
@property (weak, nonatomic) UILabel *labelStudents;

@end

@implementation ListingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
