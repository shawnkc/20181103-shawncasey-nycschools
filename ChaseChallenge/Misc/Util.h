//
//  Util.h
//  CarBingo
//
//  Created by Shawn on 5/10/18.
//  Copyright © 2018 Solitude Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define FROM_MICROS(x)              (x / 1000000.0)
#define TO_MICROS(x)                (x * 1000000.0)
#define TO_RADIANS(x)               (x * M_PI / 180.0f)

#define IS_OS_5_OR_LATER            ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0)
#define IS_OS_6_OR_LATER            ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
#define IS_OS_7_OR_LATER            ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_OS_8_OR_LATER            ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IS_OS_9_OR_LATER            ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define IS_OS_10_OR_LATER            ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
#define IS_OS_11_OR_LATER            ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)
#define IS_OS_12_OR_LATER            ([[[UIDevice currentDevice] systemVersion] floatValue] >= 12.0)

#define IS_NUMBER(x)                (x && [x isKindOfClass:[NSNumber class]])
#define IS_STRING(x)                (x && [x isKindOfClass:[NSString class]])
#define IS_ARRAY(x)                 (x && [x isKindOfClass:[NSArray class]])
#define IS_DICTIONARY(x)            (x && [x isKindOfClass:[NSDictionary class]])
#define NSNUMBER_OR_ZERO(x)         (IS_NUMBER(x) ? x : @0)
#define NSNUMBER_OR_ONE(x)          (IS_NUMBER(x) ? x : @1)
#define NSNUMBER_OR_NIL(x)          (IS_NUMBER(x) ? x : nil)
#define NSSTRING_OR_EMPTY(x)        (IS_STRING(x) ? x : @"")
#define NSDICTIONARY_OR_EMPTY(x)    (IS_DICTIONARY(x) ? x : @{})
#define NSARRAY_OR_EMPTY(x)         (IS_ARRAY(x) ? x : @[])
#define IS_STRING_SET(x)            (IS_STRING(x) && ![x isEqualToString:@""])

#define NSSTRING(x)                 [NSString stringWithUTF8String:x]

#define B_TO_KB(x)                  (x / 1024.0f)
#define B_TO_MB(x)                  (x / 1024.0f / 1024.0f)

#define UIColorFromRGB(rgbValue)    [UIColor colorWithRed:((float)((rgbValue & 0xff0000) >> 16)) / 255.0f green:((float)((rgbValue & 0x00ff00) >> 8)) / 255.0f blue:((float)(rgbValue & 0x0000ff)) / 255.0f alpha:1.0f]
#define UIColorFromRGBA(rgbValue)   [UIColor colorWithRed:((float)((rgbValue & 0xff000000) >> 24)) / 255.0f green:((float)((rgbValue & 0x00ff0000) >> 16)) / 255.0f blue:((float)((rgbValue & 0x0000ff00) >> 8)) / 255.0f alpha:((float)(rgbValue & 0x0000ff)) / 255.0f]

#define REPLACE_FONT(__font_object__, __font_name__) \
__font_object__ = [UIFont fontWithName:__font_name__ size:__font_object__.pointSize]
#define REPLACE_FONT_WITH_SIZE(__font_object__, __font_name__, __size__) \
__font_object__ = [UIFont fontWithName:__font_name__ size:__size__]

@interface Util : NSObject

+ (NSDictionary *)loadPlistFromBundle:(NSString *)plist;

+ (NSString *)getUuid;
+ (void)printFonts;
+ (NSString *)getShortTimestamp;
+ (NSString *)getFormattedTime:(NSDate *)date;
+ (NSString *)getDocumentsDirectory;
//+ (NSAttributedString *)getLastUpdatedAttributedString:(NSDate *)date;
+ (NSString *)getDocumentsDirectoryWithFilename:(NSString *)filename;
+ (NSString *)getPathFromBundle:(NSString *)filename;
+ (BOOL)deleteFile:(NSString *)filename;
+ (void)recursiveFontReplacement:(UIView *)view fontName:(NSString *)fontName;
+ (void)recursiveFontReplacement:(UIView *)view fontName:(NSString *)fontName fontSize:(CGFloat)fontSize fontColor:(UIColor *)fontColor;
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;
+ (NSNumber *)microsToDouble:(NSNumber *)value;
+ (NSArray *)parseCleanColumnsFromCSVLine:(NSString *)input;

#pragma mark - JSON conversions

+ (NSDictionary *)dictionaryWithString:(NSString *)string;
+ (NSDictionary *)dictionaryWithData:(NSData *)data;
+ (NSArray *)arrayWithString:(NSString *)string;
+ (NSString *)JSONFromArray:(NSArray *)array;
+ (NSString *)JSONFromDictionary:(NSDictionary *)dictionary;

#pragma mark - Data Conversions

+ (NSData *)dataFromString:(NSString *)string;
+ (NSString *)stringFromData:(NSData *)data;

@end

inline void delay(NSTimeInterval delay, dispatch_block_t block);
