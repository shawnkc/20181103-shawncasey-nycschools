//
//  Util.m
//  CarBingo
//
//  Created by Shawn on 5/10/18.
//  Copyright © 2018 Solitude Software. All rights reserved.
//

#import "Util.h"

inline void delay(NSTimeInterval delay, dispatch_block_t block) {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), block);
}

@implementation Util

+ (NSDictionary *)loadPlistFromBundle:(NSString *)plist {
    NSDictionary *data = nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"sounds" ofType:@"plist"];
    if (path) {
        data = [[NSDictionary alloc] initWithContentsOfFile:path];
    }
    return data;
}

+ (NSData *)dataFromString:(NSString *)string {
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    return data;
}

+ (NSString *)stringFromData:(NSData *)data {
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+ (NSString *)getUuid {
    return [[NSUUID UUID] UUIDString];
}

+ (void)printFonts {
    for (NSString* family in [UIFont familyNames]) {
        NSLog(@"%@", family);
        for (NSString* name in [UIFont fontNamesForFamilyName: family]) {
            NSLog(@"  %@", name);
        }
    }
}

+ (NSString *)getShortTimestamp {
    time_t now;
    time(&now);
    char cstr[256];
    strftime(cstr, sizeof(cstr), "%m-%dT%T", gmtime(&now));
    
    NSString *str = [NSString stringWithUTF8String:cstr];
    return str;
}

+ (NSString *)getFormattedTime:(NSDate *)date {
    if (!date) {
        return @"None";
    }
    return [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
}

+ (NSString *)getDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

//+ (NSAttributedString *)getLastUpdatedAttributedString:(NSDate *)date {
//    NSString *str = [NSString stringWithFormat:@"Last updated: %@", [Util getFormattedTime:date]];
//    return [[NSAttributedString alloc] initWithString:str
//                                           attributes:@{UITextAttributeTextColor: COLOR_BODY_BASE,
//                                                        UITextAttributeFont: [UIFont fontWithName:FONT_CUSTOM_REGULAR size:CELL_SUBTITLE_FONT_SIZE]}];
//}

+ (NSString *)getDocumentsDirectoryWithFilename:(NSString *)filename {
    NSString *documentsDirectory = [self getDocumentsDirectory];
    NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:filename];
    return localFilePath;
}

+ (NSString *)getPathFromBundle:(NSString *)filename {
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:[filename stringByDeletingPathExtension] ofType:[filename pathExtension]];
    return bundlePath;
}

+ (BOOL)deleteFile:(NSString *)filename {
    if ([[NSFileManager defaultManager] isDeletableFileAtPath:filename]) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:filename error:&error];
        if (error != nil) {
            return TRUE;
        }
    }
    return FALSE;
}

+ (void)recursiveFontReplacement:(UIView *)view fontName:(NSString *)fontName {
    void (^impl)(UIView *, NSString *) = ^(UIView *view, NSString *fontName) {
        if ([view isKindOfClass:[UILabel class]]) {
            REPLACE_FONT(((UILabel *)view).font, fontName);
        } else if ([view isKindOfClass:[UIButton class]]) {
            REPLACE_FONT(((UIButton *)view).titleLabel.font, fontName);
        } else if ([view isKindOfClass:[UITextView class]]) {
            REPLACE_FONT(((UITextView *)view).font, fontName);
        }
    };
    
    impl(view, fontName);
    for (UIView *subview in view.subviews) {
        [self recursiveFontReplacement:subview fontName:fontName];
    }
}

+ (void)recursiveFontReplacement:(UIView *)view fontName:(NSString *)fontName fontSize:(CGFloat)fontSize fontColor:(UIColor *)fontColor {
    void (^impl)(UIView *, NSString *, CGFloat, UIColor *) = ^(UIView *view, NSString *fontName, CGFloat fontSize, UIColor *fontColor) {
        if ([view isKindOfClass:[UILabel class]]) {
            REPLACE_FONT_WITH_SIZE(((UILabel *)view).font, fontName, fontSize);
            ((UILabel *)view).textColor = fontColor;
        } else if ([view isKindOfClass:[UIButton class]]) {
            REPLACE_FONT_WITH_SIZE(((UIButton *)view).titleLabel.font, fontName, fontSize);
            ((UIButton *)view).titleLabel.textColor = fontColor;
        } else if ([view isKindOfClass:[UITextView class]]) {
            REPLACE_FONT_WITH_SIZE(((UITextView *)view).font, fontName, fontSize);
            ((UITextView *)view).textColor = fontColor;
        }
    };
    
    impl(view, fontName, fontSize, fontColor);
    for (UIView *subview in view.subviews) {
        [self recursiveFontReplacement:subview fontName:fontName fontSize:fontSize fontColor:fontColor];
    }
}

//+ (void)showError:(NSString *)title message:(NSString *)message {
//    [Util showMessage:(IS_STRING_SET(title) ? title : @"Error") message:message];
//}
//
//+ (void)showMessage:(NSString *)title message:(NSString *)message {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        UIAlertView *dialog = [[UIAlertView alloc] initWithTitle:NSSTRING_OR_EMPTY(title) message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [dialog show];
//    });
//}


+ (UIColor *)colorWithHexString:(NSString *)stringToConvert {
    NSString *noHashString = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""]; // remove the #
    NSScanner *scanner = [NSScanner scannerWithString:noHashString];
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]]; // remove + and $
    
    unsigned hex;
    if (![scanner scanHexInt:&hex]) {
        return nil;
    }
    
    int r = (hex >> 24) & 0xFF;
    int g = (hex >> 16) & 0xFF;
    int b = (hex >> 8) & 0xFF;
    int a = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:a / 255.0f];
}

+ (NSNumber *)microsToDouble:(NSNumber *)value {
    NSNumber *newValue = nil;
    if ([value isKindOfClass:[NSNumber class]]) {
        newValue = [NSNumber numberWithDouble:([value doubleValue] / 1000000.0)];
    }
    return newValue;
}

// Takes into account " characters and removes spaces from end and beginning of columns
+ (NSArray *)parseCleanColumnsFromCSVLine:(NSString *)input {
    NSMutableArray *cleanedColumns = [NSMutableArray new];

    // Allocate off the stack, assuming a line is small enough for this temp allocation
    // Using +1 to get the null byte
    char *bytesCopy = alloca(input.length + 1);
    memcpy(bytesCopy, input.UTF8String, input.length + 1);
    
    char *pos = bytesCopy;
    char *start = bytesCopy;
    bool isQuoted = false;
    bool commaHit = false;
    NSString *column = nil;
    
    while (*pos != '\0') {
        if (*pos == ',') {
            commaHit = true;
            
            if (pos == start) {
                column = @"";
            } else if (isQuoted) {
                // skip, do nothing
            } else {
                *pos = '\0';
                column = [NSString stringWithUTF8String:start];
            }
        } else if (*pos == '\"') {
            if (isQuoted) {
                isQuoted = false;
                *pos = '\0';
                column = [NSString stringWithUTF8String:start];
                // get past the temp EOL
                pos++;

                // Go to next , or EOL
                while (*pos != '\0') {
                    if (*pos == ',') {
                        break;
                    }
                    pos++;
                }
            } else {
                isQuoted = true;
                pos++;
                start = pos;
            }
        }

        pos++;

        if (column) {
            [cleanedColumns addObject:[column stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            column = nil;
            start = pos;
        }        
    }
    
    if (pos == start) {
        if (commaHit) {
            column = @"";
        }
    } else if (isQuoted) {
        NSLog(@"Error in parsing, malformed quotes");
    } else {
        *pos = '\0';
        column = [NSString stringWithUTF8String:start];
        // get past the temp EOL
        pos++;
    }
    
    if (column) {
        [cleanedColumns addObject:[column stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        column = nil;
        start = pos;
    }
    
    return cleanedColumns;
}

+ (NSArray *)readCleanColumnsFromCSVLine2:(NSString *)input {
    NSArray *columns = [input componentsSeparatedByString:@","];
    NSMutableArray *cleanedColumns = [[NSMutableArray alloc] initWithCapacity:columns.count];
    
    NSMutableString *partialColumn = nil;
    for (int i = 0; i < columns.count; i++) {
        NSString *column = columns[i];
        
        if ([column characterAtIndex:0] == '\"') {
            partialColumn = [NSMutableString stringWithString:[column substringFromIndex:1]];
            continue;
        } else if (partialColumn) {
            [partialColumn appendString:column];
            if ([column characterAtIndex:column.length] != '\"') {
                continue;
            }
            column = partialColumn;
            partialColumn = nil;
        }
        
        [cleanedColumns addObject:column];
    }
    
    return cleanedColumns;
}

#pragma JSON conversions

+ (NSDictionary *)dictionaryWithString:(NSString *)string {
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    id json = nil;
    if (data) {
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    }
    return json;
}

+ (NSDictionary *)dictionaryWithData:(NSData *)data {
    NSDictionary *dictionary = nil;
    if (data) {
        NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if (json) {
            dictionary = [Util dictionaryWithString:json];
        }
    }
    return dictionary;
}

+ (NSArray *)arrayWithString:(NSString *)string {
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    id json = nil;
    if (data) {
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    }
    return json;
}

+ (NSString *)JSONFromDictionary:(NSDictionary *)dictionary {
    NSError *error;
    NSData *jsonData = nil;
    if (dictionary) {
        jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                   options:0 // Pass 0 if you don't care about the readability of the generated string
                                                     error:&error];
    }
    
    if (jsonData) {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return nil;
}

+ (NSString *)JSONFromArray:(NSArray *)array {
    NSError *error;
    // Try converting native objects to JSON first
    NSMutableArray *convertedArray = @[].mutableCopy;
    
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wundeclared-selector"
#pragma GCC diagnostic ignored "-Wselector"
    for (NSObject *item in array) {
        if ([item respondsToSelector:@selector(toDictionary)]) {
            [convertedArray addObject:[item performSelector:@selector(toDictionary)]];
        } else if ([item respondsToSelector:@selector(dictionary)]) {
            [convertedArray addObject:[item performSelector:@selector(dictionary)]];
        } else {
            [convertedArray addObject:item];
        }
    }
#pragma clang diagnostic pop
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:convertedArray
                                                       options:0 // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (jsonData) {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return nil;
}

@end
