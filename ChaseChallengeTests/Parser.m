//
//  Parser.m
//  ChaseChallengeTests
//
//  Created by Shawn on 11/2/18.
//  Copyright © 2018 Shawn. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Util.h"

@interface Parser : XCTestCase

@end

@implementation Parser

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testQuotesAndSpaces {
    NSArray *cleanedColumns = nil;

    cleanedColumns = [Util parseCleanColumnsFromCSVLine:@"a,b,c"];
    XCTAssertTrue(cleanedColumns.count == 3);
    XCTAssertEqualObjects(cleanedColumns[0], @"a");
    XCTAssertEqualObjects(cleanedColumns[1], @"b");
    XCTAssertEqualObjects(cleanedColumns[2], @"c");
    
    cleanedColumns = [Util parseCleanColumnsFromCSVLine:@"a, \"b, or x\", c"];
    XCTAssertTrue(cleanedColumns.count == 3);
    XCTAssertEqualObjects(cleanedColumns[0], @"a");
    XCTAssertEqualObjects(cleanedColumns[1], @"b, or x");
    XCTAssertEqualObjects(cleanedColumns[2], @"c");

    cleanedColumns = [Util parseCleanColumnsFromCSVLine:@" a, \"b, or x\" , c "];
    XCTAssertTrue(cleanedColumns.count == 3);
    XCTAssertEqualObjects(cleanedColumns[0], @"a");
    XCTAssertEqualObjects(cleanedColumns[1], @"b, or x");
    XCTAssertEqualObjects(cleanedColumns[2], @"c");
    
    cleanedColumns = [Util parseCleanColumnsFromCSVLine:@" a, c, "];
    XCTAssertTrue(cleanedColumns.count == 3);
    XCTAssertEqualObjects(cleanedColumns[0], @"a");
    XCTAssertEqualObjects(cleanedColumns[1], @"c");
    XCTAssertEqualObjects(cleanedColumns[2], @"");

    cleanedColumns = [Util parseCleanColumnsFromCSVLine:@"a,b,,,,,c"];
    XCTAssertTrue(cleanedColumns.count == 7);
    XCTAssertEqualObjects(cleanedColumns[0], @"a");
    XCTAssertEqualObjects(cleanedColumns[1], @"b");
    XCTAssertEqualObjects(cleanedColumns[6], @"c");

    cleanedColumns = [Util parseCleanColumnsFromCSVLine:@""];
    XCTAssertTrue(cleanedColumns.count == 0);

    cleanedColumns = [Util parseCleanColumnsFromCSVLine:@","];
    XCTAssertTrue(cleanedColumns.count == 2);
    XCTAssertEqualObjects(cleanedColumns[0], @"");
    XCTAssertEqualObjects(cleanedColumns[1], @"");
}

@end
